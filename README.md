# Reddit Redesign

Custom CSS for Reddit. Only available for old.reddit.com design. Recommended for lurkers with or without accounts.

## Preview

### Front page
![frontpage](images/frontpage.png)

### Subreddit
![subreddit](images/subreddit.png)

### Thread
![thread](images/thread.png)

## Precaution

* This CSS is not written to integrate with custom CSS for some subreddits. Either disable this CSS or leave the checkbox for use subreddit style at the sidebar blank. 
- Install stylus/stylish to use css.
* frontpage.css is for the front page and subreddit pages. subreddit.css is for selected threads. reddit-user is for the user pages (not profile page).
- This css has only been tested on Firefox and Waterfox.


## Latest

* Removed the vote buttons
- Increased the width of post boxes
* Adjusted border margins so that the front page margins is different from the subreddit and thread margins.

## Plans
* Change colour scheme
- Edit css files to reduce redundancy